package contexts;

import environnment.Road;
import repast.simphony.context.DefaultContext;

public class RoadContext extends DefaultContext<Road> {

	public RoadContext() {
		super("RoadContext");
	}

}
