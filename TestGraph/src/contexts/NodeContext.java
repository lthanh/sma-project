package contexts;

import environnment.Node;
import repast.simphony.context.DefaultContext;

public class NodeContext extends DefaultContext<Node> {

	public NodeContext()
	{
		super("NodeContext");
	}

}
