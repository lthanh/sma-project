package testGraph;


import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import com.vividsolutions.jts.geom.GeometryFactory;

import agent.Agent;
import agent.Scheduler;
import repast.simphony.context.Context;
import repast.simphony.context.DefaultContext;
import repast.simphony.context.space.gis.GeographyFactoryFinder;
import repast.simphony.dataLoader.ContextBuilder;
import repast.simphony.engine.environment.RunEnvironment;
import repast.simphony.space.gis.Geography;
import repast.simphony.space.gis.GeographyParameters;
import repast.simphony.space.gis.SimpleAdder;
import contexts.NodeContext;
import contexts.RoadContext;
import edu.uci.ics.jung.algorithms.shortestpath.DijkstraShortestPath;
import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import environnment.*;

public class Graph implements ContextBuilder<Object> {

  private static Context<Object> mainContext;

  private static UndirectedSparseGraph<Node, CustomEdge> roadNetwork;

  private static DijkstraShortestPath<Node, CustomEdge> alg;

  private static Geography<Road> roadProjection;

  private static Geography<Agent> agentGeography;
  private static Scheduler Scheduler;

  public static CustomTransformer customTransformer;

  public static int carNumber;
  private static int gpsPercentage;
  public static int signPercentage;


  @Override
  public Context<Object> build(Context<Object> context) {
    carNumber = RunEnvironment.getInstance().getParameters().getInteger("carNumber");
    gpsPercentage = RunEnvironment.getInstance().getParameters().getInteger("gpsPercentage");
    gpsPercentage = Math.max(Math.min(gpsPercentage, 100), 0);
    signPercentage = RunEnvironment.getInstance().getParameters().getInteger("signPercentage");
    signPercentage = Math.max(Math.min(signPercentage, 100), 0);

    mainContext = context;
    mainContext.setId("mainContext");

    RoadContext roadContext = new RoadContext();
    roadProjection = GeographyFactoryFinder.createGeographyFactory(null).createGeography(
        "RoadGeography", roadContext,
        new GeographyParameters<Road>(new SimpleAdder<Road>()));

    String roadFile = "./data/gis_data/california_main_roads/PrimaryAndSecondaryRoads.shp";
    //String roadFile = "./data/gis_data/toy_city/roads.shp";

    try {
      GISFunctions.readShapefile(Road.class, roadFile, roadProjection, roadContext);
    } catch (MalformedURLException | FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    mainContext.addSubContext(roadContext);
    SpatialIndexManager.createIndex(roadProjection, Road.class);

    // Create road network

    // 1.junctionContext and junctionGeography
    NodeContext nodeContext = new NodeContext();
    mainContext.addSubContext(nodeContext);

    Geography<Node> nodeGeography = GeographyFactoryFinder.createGeographyFactory(null).createGeography(
        "NodeGeography", nodeContext,
        new GeographyParameters<Node>(new SimpleAdder<Node>()));

    // 2. roadNetwork
    //    NetworkBuilder<NodeE> builder = new NetworkBuilder<NodeE>("RoadNetwork",
    //        nodeContext, false);
    //    builder.setEdgeCreator(new CustomEdgeCreator());
    //    Network<NodeE> roadNetwork = builder.buildNetwork();
    roadNetwork = new UndirectedSparseGraph<Node, CustomEdge>();

    GISFunctions.buildGISRoadNetwork(roadProjection, nodeContext, nodeGeography, roadNetwork);

    customTransformer = new CustomTransformer();

    alg = new DijkstraShortestPath<>(roadNetwork, customTransformer);

    // Add the junctions to a spatial index (couldn't do this until the
    // road network had been created).
    SpatialIndexManager.createIndex(nodeGeography, Node.class);


    // Create the agents
    Context<Agent> agentContext = new DefaultContext<Agent>("AgentContext");
    mainContext.addSubContext(agentContext);

    agentGeography = GeographyFactoryFinder.createGeographyFactory(null).createGeography(
        "AgentGeography", agentContext, new GeographyParameters<Agent>(new SimpleAdder<Agent>()));

    ArrayList<Agent> agents = new ArrayList<Agent>();
    for (int i = 0; i < carNumber; ++i) {
      Agent a = new Agent(roadNetwork, gpsPercentage == 0 ? false : i <= carNumber * gpsPercentage / 100);
      agentContext.add(a);
      agentGeography.move(a, (new GeometryFactory()).createPoint(a.getOrig().getCoords()));
      agents.add(a);
    }

    Scheduler = new Scheduler(agents);
    mainContext.add(Scheduler);

    return mainContext;
  }

  public  UndirectedSparseGraph<Node, CustomEdge> getNetwork() {
    return roadNetwork;
  }

  public static DijkstraShortestPath<Node, CustomEdge> getDijkstra() {
    return alg;
  }

  public static Geography<Road> getRoadProjection() {
    return roadProjection;
  }

  public static Geography<Agent> getAgentGeography() {
    return agentGeography;
  }
}

