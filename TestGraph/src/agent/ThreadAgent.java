package agent;

public class ThreadAgent extends Thread {

	private int cpuNumber;
	private Controller controller;
	private Agent theAgent;
	private boolean go = false;
	
	public ThreadAgent(Controller controller, int cpuNumber, Agent b)
	{
		this.controller = controller;
		this.cpuNumber = cpuNumber;
		this.theAgent = b;
	}
	
	public ThreadAgent(Controller controller)
	{
		this.controller = controller;
	}
	
	public void setAgent(Agent a)
	{
		theAgent = a;
	}
	

	public void setIndex(int i) {
		this.cpuNumber = i;
	}
	
	@Override
	public synchronized void run() {
		while(!go)
		{
			try {wait();}
			catch (InterruptedException e) {e.printStackTrace();}
			try {
				if(this.theAgent!=null)
					this.theAgent.step();
				this.theAgent = null;
			} catch (Exception ex) {ex.printStackTrace();}
			controller.setCPUFree(cpuNumber);
			go = false;
		}
	}
	
	public synchronized void go(){
		go = true;
		notifyAll();
	}
}