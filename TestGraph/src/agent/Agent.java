package agent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import org.geotools.referencing.GeodeticCalculator;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;

import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import environnment.CustomEdge;
import environnment.Node;
import repast.simphony.context.Context;
import repast.simphony.space.gis.Geography;
import repast.simphony.util.ContextUtils;
import testGraph.Graph;

public class Agent {

  UndirectedSparseGraph<Node, CustomEdge> roadNetwork;

  Node orig;
  Node target; // current target
  Node dest; // final node of the path
  int origIdx;
  int destIdx;

  List<CustomEdge> path;
  List<Coordinate> coords;

  double speed = 0.01;
  final double maxSpeed = 0.01;
  public final static double MAXSPEED = 0.01;

  int b = 0;
  boolean inverted = false;
  boolean first = true;
  boolean none = true;
  static final Random r = new Random();
  boolean gps = false;

  int b_gps = 0;

  public int getB_gps() {
    return b_gps;
  }

  public void setB_gps(int b_gps) {
    this.b_gps = b_gps;
  }

  public boolean getGps() {
    return gps;
  }


  public double getSpeed() {
    return speed;
  }

  public void setSpeed(double speed) {
    this.speed = speed;
  }

  public Agent(UndirectedSparseGraph<Node, CustomEdge> roadNetwork, boolean gps) {
    this.roadNetwork = roadNetwork;
    this.createRandomPath();
    this.coords = new ArrayList<Coordinate>(Arrays.asList(this.path.get(0).getCoords()));
    this.directCoordinates();
    coords.remove(0);
    this.path.get(0).incrCarCount();
    this.setGps(gps);
  }

  public void step() {
    Context<Agent> context = ContextUtils.getContext(this);
    Geography<Agent> geography = (Geography<Agent>)context.getProjection("AgentGeography");

    Geometry geom = geography.getGeometry(this);
    Coordinate coord = geom.getCoordinate();

    speed = maxSpeed / (1.0 + (double)this.path.get(0).getCarCount() / 10);
    double dist = speed;

    boolean stop = false;
    while (!stop && this.coords.size() != 0) {
      double distBetweenCoords = distance(coord, coords.get(0), geography);
      //      if (distBetweenCoords <= dist) {
      dist -= distBetweenCoords;

      coord.x = this.coords.get(0).x;
      coord.y = this.coords.get(0).y;
      this.coords.remove(0);

      if (this.coords.size() == 0)
        changeEdge();

      if (dist <= 0 || this.coords.size() == 0) {
        synchronized (geography) {
          geography.move(this, geom);
        }
        stop = true;
      }
      //      } else {
      //        double angle = angle(coord, this.coords.get(0), geography);
      //        coord.x += dist * Math.cos(angle);
      //        coord.y += dist * Math.sin(angle);
      //        synchronized (geography) {
      //          //          geom = geography.moveByVector(this, dist * 10000, angle);
      //          geography.move(this, geom);
      //        }
      //        stop = true;
      //      }
    }
  }

  // changement d'arc (et parfois de path)
  private void changeEdge() {
    if (this.coords.size() == 0) {
      this.path.get(0).decrCarCount();
      this.path.remove(0);
      if (this.path.size() == 0)
      {
        this.updateRandomPath();
      }
      //else if (isGps() || this.target.isInformation())
      //	  path = ShortestPath();
      else if (isGps())
      {
        setOrig(this.target);
        path = ShortestPath();
      }
      this.coords = new ArrayList<Coordinate>(Arrays.asList(this.path.get(0).getCoords()));
      this.directCoordinates();
      this.coords.remove(0);
      this.path.get(0).incrCarCount();
    }
  }

  private void createRandomPath() {
    do {
      origIdx = r.nextInt(roadNetwork.getVertexCount());
      this.orig = (Node)(roadNetwork.getVertices().toArray()[origIdx]);

      destIdx = r.nextInt(roadNetwork.getVertexCount());
      this.dest = (Node)(roadNetwork.getVertices().toArray()[destIdx]);

      path = ShortestPath();
    } while (path.size() <= 5);
  }

  private void updateRandomPath() {
    origIdx = destIdx;
    this.orig = (Node)(roadNetwork.getVertices().toArray()[origIdx]);

    do {
      destIdx = r.nextInt(roadNetwork.getVertexCount());
      this.dest = (Node)(roadNetwork.getVertices().toArray()[destIdx]);

      path = ShortestPath();
    } while (path.size() <= 5);
  }

  public Node getOrig() {
    return orig;
  }

  public void setOrig(Node orig) {
    this.orig = orig;
  }

  public List<CustomEdge> ShortestPath()
  {
    List<CustomEdge> res;
    synchronized (Graph.getDijkstra()) {
      res = Graph.getDijkstra().getPath(orig, dest);
    }
    return res;
  }

  public void directCoordinates()
  {
    if (distance(this.orig.getCoords(), this.coords.get(0), null) >
    distance(this.orig.getCoords(), this.coords.get(this.coords.size()-1), null)) {
      Collections.reverse(this.coords);
      this.orig = this.path.get(0).getTarget();
      this.target = this.path.get(0).getSource();
    } else {
      this.orig = this.path.get(0).getSource();
      this.target = this.path.get(0).getTarget();
    }
  }

  public double angle(Coordinate c1, Coordinate c2, Geography<Agent> geography)
  {
    // C'est la trigo ici!!!!!!
    //	  // On peut simplifier mais c pour etre clair
    //    double hypothenuse = distance(c1, c2, geography);
    //    double opposite = Math.abs(c1.y - c2.y);
    //    double sinusA = opposite / hypothenuse;
    //    double angle = Math.asin(sinusA);
    //
    //    return angle;

    GeodeticCalculator calculator = new GeodeticCalculator(geography.getCRS());
    calculator.setStartingGeographicPoint(c1.x, c1.y);
    calculator.setDestinationGeographicPoint(c2.x, c2.y);
    double angle = Math.toRadians(calculator.getAzimuth()); // Angle in range -PI to PI
    // Need to transform azimuth (in range -180 -> 180 and where 0 points north)
    // to standard mathematical (range 0 -> 360 and 90 points north)
    if (angle > 0 && angle < 0.5 * Math.PI) { // NE Quadrant
      angle = 0.5 * Math.PI - angle;
    } else if (angle >= 0.5 * Math.PI) { // SE Quadrant
      angle = (-angle) + 2.5 * Math.PI;
    } else if (angle < 0 && angle > -0.5 * Math.PI) { // NW Quadrant
      angle = (-1 * angle) + 0.5 * Math.PI;
    } else { // SW Quadrant
      angle = -angle + 0.5 * Math.PI;
    }
    return angle;
  }

  public double distance(Coordinate c1, Coordinate c2,Geography<Agent> geography)
  {

    // Apparament, on peut calculer la distance en metre avec �a
    // La distance entre deux Coordinate (c1.distance(c2)) n'est pas la m�me que cette distance (test�)
    //    GeodeticCalculator calculator = new GeodeticCalculator(geography.getCRS());
    //    calculator.setStartingGeographicPoint(c1.x, c1.y);
    //    calculator.setDestinationGeographicPoint(c2.x, c2.y);
    //    double res = calculator.getOrthodromicDistance();

    double res = c1.distance(c2);
    return res > 0 ? res : -res;
  }

  public ArrayList<Coordinate> nextCoordinate(Coordinate c1, Coordinate c2, double precision)
  {

    ArrayList<Coordinate> res = new ArrayList<Coordinate>();

    double coefD = (c1.y - c2.y)/(c1.x - c2.x);
    double ord = c1.y - coefD*c1.x;

    for (double i = c1.x ; i <= c1.y; i += precision)
      res.add(new Coordinate(i, i * coefD + ord));

    return res;
  }

  public boolean isGps() {
    return gps;
  }

  public void setGps(boolean gps) {
    this.gps = gps;
    b_gps = gps ? 1 : 0;
  }

}

