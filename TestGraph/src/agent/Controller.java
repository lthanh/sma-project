package agent;

import java.util.List;

public class Controller extends Thread{

	private List<Agent> agents;
	
	private int numCPUs;
	private boolean go = false;
	private Scheduler scheduler;
	private ThreadAgent[] threads;
	private boolean[] cpuStatus;
	
	public Controller(Scheduler scheduler, List<Agent> agents) {
		this.agents = agents;
		this.scheduler = scheduler;
		this.numCPUs = Runtime.getRuntime().availableProcessors();
		this.cpuStatus = new boolean[this.numCPUs];
		this.threads = new ThreadAgent[numCPUs];
		for (int i = 0; i < this.numCPUs; i++)
		{
			this.cpuStatus[i] = true;
			this.threads[i]= new ThreadAgent(this);
			this.threads[i].start();
		}
	}

	public synchronized void go(){
		go = true;
		notifyAll();
	}
	
	@Override
	public synchronized void run() {
		while(!go)
		{ 
			try { this.wait(); }
			catch (InterruptedException e) {e.printStackTrace();}

			for (Agent a : agents)
			{
				boolean foundFreeCPU = false;
		
				while (!foundFreeCPU)
				{
					cpus : for (int i = 0; i < this.numCPUs; i++) {
						if (this.cpuStatus[i]) {
							foundFreeCPU = true;
							this.cpuStatus[i] = false;
							
							threads[i].setAgent(a);
							threads[i].setIndex(i);
							threads[i].go();
							break cpus;
						}
					}
					
					if (!foundFreeCPU) {
						this.waitForAgentThread();
					}
				}
			}
			
			
			boolean allFinished = false;
			while (!allFinished) {
				allFinished = true;
			
				cpus : for (int i = 0; i < this.cpuStatus.length; i++)
				{
					if (!this.cpuStatus[i]) {
						allFinished = false;
						break cpus;
					}
				}
			if (!allFinished)
				{ this.waitForAgentThread(); }
		}

		this.scheduler.setAgentsFinishedStepping();
		go = false;
		}
	}

	public synchronized void setCPUFree(int cpuNumber) {
		this.cpuStatus[cpuNumber] = true;
		this.notify();
	}
	
	private synchronized void waitForAgentThread() {
		try
		{
			this.wait();
		}
		catch (InterruptedException e)
		{
			e.printStackTrace();
		}
	}
}