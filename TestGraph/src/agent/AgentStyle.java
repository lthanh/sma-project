package agent;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.render.BasicWWTexture;

import java.net.URL;

import repast.simphony.visualization.gis3D.style.DefaultMarkStyle;

public class AgentStyle extends DefaultMarkStyle<Agent> {

  @Override
  public BasicWWTexture getTexture(Agent object, BasicWWTexture texture) {

    if (texture != null)
      return texture;

    URL localUrl = WorldWind.getDataFileStore().requestFile("./data/icons/car3.png");
    if (localUrl != null) {
      System.out.println("OK");
      return new BasicWWTexture(localUrl, false);
    }
    System.out.println("KO");
    return null;
  }
}
