package agent;

import java.util.List;

import repast.simphony.engine.schedule.ScheduledMethod;

public class Scheduler {

	private boolean agentsFinishedStepping;
	private Controller threadController;
	public Scheduler(List<Agent> agents) {
	super();
		threadController = new Controller(this, agents);
		threadController.start();
	}
	
	@ScheduledMethod(start = 1, interval = 1)
	public synchronized void schedule()
	{
		this.agentsFinishedStepping = false;
		this.threadController.go();
		while (!this.agentsFinishedStepping)
		{
			try {
				this.wait(); // Wait for the ThreadController to call setagentsFinishedStepping()
			}
				catch (InterruptedException e) {e.printStackTrace();
			}
		}
	}

	public synchronized void setAgentsFinishedStepping() {
		this.agentsFinishedStepping = true;
		this.notifyAll();
	}
	
}

