package environnment;

import org.apache.commons.collections15.Transformer;

public class CustomTransformer implements Transformer<CustomEdge, Double> {

  private boolean gps = false;

  public CustomTransformer() {
    this.gps = false;
  }

  public CustomTransformer(boolean gps) {
    this.gps = gps;
  }

  public void set_gps (boolean gps){
    this.gps = gps;
  }

  @Override
  public Double transform(CustomEdge edge) {
    if (gps)
      return edge.getWeight() + (edge.getCarCount() * edge.getWeight() / 10);
    else
      return edge.getWeight();
  }

}
