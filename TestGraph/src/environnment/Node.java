package environnment;

import com.vividsolutions.jts.geom.Coordinate;

public class Node implements FixedGeography {
	
	private Coordinate coord;
	private boolean information;
	
	public Node() {
		this.information = false;
	}

	public Node(boolean information) {
		this.information = information;
	}
	
	@Override
	public Coordinate getCoords() {
		return coord;
	}

	@Override
	public void setCoords(Coordinate c) {
		this.coord = c;
	}

	public boolean isInformation() {
		return information;
	}

	public void setInformation(boolean information) {
		this.information = information;
	}
}
