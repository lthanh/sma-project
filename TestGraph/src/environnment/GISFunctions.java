/*
Copyright 2012 Nick Malleson
This file is part of RepastCity.

RepastCity is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

RepastCity is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with RepastCity.  If not, see <http://www.gnu.org/licenses/>.
 */

package environnment;

import java.io.File;
import java.io.FileNotFoundException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.Point;

import edu.uci.ics.jung.graph.UndirectedSparseGraph;
import repast.simphony.context.Context;
import repast.simphony.space.gis.Geography;
import repast.simphony.space.gis.ShapefileLoader;
import testGraph.Graph;

/**
 * Class with useful GIS functions for configuring the GIS model environment.
 * 
 * @author Nick Malleson
 * 
 */
public class GISFunctions {



  /**
   * Create the road network. Runs through the roads in the <code>roadGeography</code> and, for each one, will create
   * <code>Node</code> objects at their end points and an edge linking them. The <code>Node</code> objects are
   * added to the given <code>NodeGeography</code> (so that we know where they are spatially) and they are also
   * added, along with the edge between them, to the <code>NodeNetwork</code> so that topographical relationships
   * can be established. (The <code>NodeNetwork</code> is part of the <code>NodeContext</code>
   * 
   * @param roadGeography
   * @param NodeContext
   * @param NodeGeography
   * @param graph
   */
  public static void buildGISRoadNetwork(Geography<Road> roadGeography, Context<Node> NodeContext,
      Geography<Node> NodeGeography, UndirectedSparseGraph<Node, CustomEdge> realGraph) {

    GeometryFactory geomFac = new GeometryFactory();
    CustomEdgeCreator edgeFactory = new CustomEdgeCreator();
    // Create a cache of all Nodes and coordinates so we know if a Node has already been created at a
    // particular coordinate
    Map<Coordinate, Node> coordMap = new HashMap<Coordinate, Node>();

    // Iterate through all roads
    Iterable<Road> roadIt = roadGeography.getAllObjects();

    int signProba = 0;
    if (Graph.signPercentage > 0)
    	signProba = 100 / Graph.signPercentage;
    Random random = new Random();
    
    for (Road road : roadIt) {
      // Create a LineString from the road so we can extract coordinates
      Geometry roadGeom = roadGeography.getGeometry(road);
      Coordinate c1 = roadGeom.getCoordinates()[0]; // First coord
      Coordinate c2 = roadGeom.getCoordinates()[roadGeom.getNumPoints() - 1]; // Last coord

      // Create Nodes from these coordinates and add them to the NodeGeography (if they haven't been
      // created already)
      Node junc1, junc2;
      if (coordMap.containsKey(c1)) {
        // A Node with those coordinates (c1) has been created, get it so we can add an edge to it
        junc1 = coordMap.get(c1);
      } else { // Node does not exit
        junc1 = new Node(random.nextInt(signProba) == 0);
        junc1.setCoords(c1);
        NodeContext.add(junc1);
        coordMap.put(c1, junc1);
        Point p1 = geomFac.createPoint(c1);
        NodeGeography.move(junc1, p1);

      }
      if (coordMap.containsKey(c2)) {
        junc2 = coordMap.get(c2);
      } else { // Node does not exit
        junc2 = new Node(random.nextInt(signProba) == 0);
        junc2.setCoords(c2);
        NodeContext.add(junc2);
        coordMap.put(c2, junc2);
        Point p2 = geomFac.createPoint(c2);
        NodeGeography.move(junc2, p2);
      }

      double distance = 0;
      for (int i = 0;  i < roadGeom.getNumPoints() -1 ; ++i)
    	  distance += roadGeom.getCoordinates()[i].distance(roadGeom.getCoordinates()[i + 1]);
          
      CustomEdge edge = edgeFactory.createEdge(junc1, junc2, false, distance);
      edge.setCoords(roadGeom.getCoordinates());

      if (!realGraph.containsEdge(edge))
        realGraph.addEdge(edge, junc1,junc2);
    } // for road:
  }


  /**
   * Nice generic function :-) that reads in objects from shapefiles.
   * <p>
   * The objects (agents) created must extend FixedGeography to guarantee that they will have a setCoords() method.
   * This is necessary because, for simplicity, geographical objects which don't move store their coordinates
   * alongside the projection which stores them as well. So the coordinates must be set manually by this function once
   * the shapefile has been read and the objects have been given coordinates in their projection.
   * 
   * @param <T>
   *            The type of object to be read (e.g. PecsHouse). Must exted
   * @param cl
   *            The class of the building being read (e.g. PecsHouse.class).
   * @param shapefileLocation
   *            The location of the shapefile containing the objects.
   * @param geog
   *            A geography to add the objects to.
   * @param context
   *            A context to add the objects to.
   * @throws MalformedURLException
   *             If the location of the shapefile cannot be converted into a URL
   * @throws FileNotFoundException
   *             if the shapefile does not exist.
   * @see FixedGeography
   */
  public static <T extends FixedGeography> void readShapefile(Class<T> cl, String shapefileLocation,
      Geography<T> geog, Context<T> context) throws MalformedURLException, FileNotFoundException {
    File shapefile = null;
    ShapefileLoader<T> loader = null;
    shapefile = new File(shapefileLocation);
    if (!shapefile.exists()) {
      throw new FileNotFoundException("Could not find the given shapefile: " + shapefile.getAbsolutePath());
    }
    loader = new ShapefileLoader<T>(cl, shapefile.toURI().toURL(), geog, context);
    while (loader.hasNext()) {
      loader.next();
    }
    for (T obj : context.getObjects(cl)) {
      obj.setCoords(geog.getGeometry(obj).getCentroid().getCoordinate());
    }
  }

  /**
   * An alternative to <code>readShapefile()</code> that does not require objects to implement
   * <code>FixedGeography<code>. Hence it can be used by objects that don't store their coordinates internally (such
   * as agents).
   * 
   * @param <T>
   *            The type of object to be read (e.g. PecsHouse). Must exted
   * @param cl
   *            The class of the building being read (e.g. PecsHouse.class).
   * @param shapefileLocation
   *            The location of the shapefile containing the objects.
   * @param geog
   *            A geography to add the objects to.
   * @param context
   *            A context to add the objects to.
   * @throws MalformedURLException
   *             If the location of the shapefile cannot be converted into a URL
   * @throws FileNotFoundException
   *             if the shapefile does not exist.
   * @see FixedGeography
   */
  public static <T> void readAgentShapefile(Class<T> cl, String shapefileLocation, Geography<T> geog,
      Context<T> context) throws MalformedURLException, FileNotFoundException {

    File shapefile = null;
    ShapefileLoader<T> loader = null;
    shapefile = new File(shapefileLocation);
    if (!shapefile.exists()) {
      throw new FileNotFoundException("Could not find the given shapefile: " + shapefile.getAbsolutePath());
    }
    loader = new ShapefileLoader<T>(cl, shapefile.toURI().toURL(), geog, context);
    while (loader.hasNext()) {
      loader.next();
    }
  }

}
