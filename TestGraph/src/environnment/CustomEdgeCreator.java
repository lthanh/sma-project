package environnment;

import repast.simphony.space.graph.*;;

public class CustomEdgeCreator implements EdgeCreator<CustomEdge, Node> {

	@Override
	public Class getEdgeType() {
		return CustomEdge.class.getClass();
	}

	@Override
	public CustomEdge createEdge(Node source, Node target,
			boolean isDirected, double weight) {
		return new CustomEdge(source,target,isDirected,weight);
	}

}
