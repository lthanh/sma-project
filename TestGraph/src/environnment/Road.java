package environnment;

import com.vividsolutions.jts.geom.Coordinate;

public class Road implements FixedGeography{
	
	private Coordinate coord;
	
	public Road() {
		
	}

	@Override
	public Coordinate getCoords() {
		return coord;
	}

	@Override
	public void setCoords(Coordinate c) {
		this.coord = c;
	}
}
