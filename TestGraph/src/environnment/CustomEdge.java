package environnment;

import com.vividsolutions.jts.geom.Coordinate;

import repast.simphony.space.graph.RepastEdge;

public class CustomEdge extends RepastEdge<Node> {
	
	private Coordinate[] coords;
	private int carCount = 0;
	
	public CustomEdge(Node source, Node target, boolean isdirected, double weight) {
		super(source, target, isdirected,weight);
		this.carCount = 0;
	}
	
	public CustomEdge(Node source, Node target, boolean isdirected, double weight, Coordinate[] coords) {
		super(source, target, isdirected,weight);
		this.coords = coords;
		this.carCount = 0;
	}
	
	public int getCarCount()
	{
		return carCount;
	}

	public void setCarCount(int carCount)
	{
		this.carCount = carCount;
	}
	
	public void incrCarCount() { ++this.carCount; }
	public void decrCarCount() { --this.carCount; }
	
	
	public void setCoords(Coordinate[] coords)
	{
		this.coords = coords;
	}
	
	public Coordinate[] getCoords()
	{
		return this.coords;
	}
}
